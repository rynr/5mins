---
title: "Git"
date: 2023-02-02T21:10:00+01:00
tags:
 - version control system
 - git
summary: Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.
---
## What is git used for?

`Git` is a free and open source distributed [version control system][vcs] designed to handle everything from small to very large projects with speed and efficiency.

### Free and Open Source?

`git` is licensed with the [GNU General Public License version 2.0][gpl2], this means you can freely share and change the software.  
The source code of `git` can be retrieved from a [`git` repository][git-repo].

### Version Control System?

Git is used to have control over changing files, like source-files in software projects. Git keeps all versions of the files in a directory and gives the possibillity to view or retrieve previous versions and to see a history of all changes.

### Distributed?

Sharing changes between stakeholders of the project is a requirement for projects. All history is stored in a git `repository`. Every participant on a project has his own `repository`. Usually all changes are shared between all participants, but every participant can decide to not share some changes.  
Git provides central hubs where every participant can synchronize his changes.  
As `repositories` may diverge over time, `git` provides tools to synchronize changes.

## More

 * [git-scm.com][git]

[git]: https://git-scm.com/
[git-repo]: https://github.com/git/git
[gpl2]: https://opensource.org/licenses/GPL-2.0
[vcs]: {{< ref "version-control-system" >}}