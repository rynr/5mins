---
title: Terraform
date: 2023-02-02T19:45:00 
tags:
 - terraform
 - cloud
 - infrastructure
summary: Terraform is an infrastructure as code tool that lets you build, change, and version cloud and on-prem resources safely and efficiently.
---
## What is terraform used for?

Terraform is used to automate cloud infrastructure as code.  
This means, you can write some configuration and terraform ensures, some cloud infrastructure aligns with the configuration given.  

## What can terraform control?

Terraform can automate pretty much any cloud or digital resource, as long as there's a provider for it.

Each provider gives access to `resources` and `data`.  
`Resources` provide information and can be configured with a desired state, while `data` only provides information.

Terraform has a registry, here you can find all public [providers][terraform-providers]. Among the most used ones are [aws][terraform-provider-aws], [azure][terraform-provider-azure], [google][terraform-provider-gcloud] or [kubernetes][terraform-provider-kubernetes].

## What is the workflow of terraform?

Terraform stores a state of all configured resources. When executed, terraform reads and validates all configuration. It checks for any difference of the resources to the state. Then terraform makes a list of changes that need to be applied to reach the configured state. This happens during the `plan` pahse.  
Then terraform runs the `apply` phase, which requests all the required changes from the providers and stores the new state.

These steps can be all done with one command, or one after another.

```goat
                              .------.
                          .--+ Status |<--.
                         |    '------'     |
                         v                 |
 .-------------.     +-----------+    +----+-----+
| Configuration +--->|    Plan   +--->|   Apply  |
 '-------------'     +-----------+    +----+-----+
                         ^                 |
                         |   .--------.    |
                          '-+ Provider |<-'
                             '----+---'
                                  |
                                  v
                             .---------.
                            | Resources |
                             '---------'
```

## More

* [terraform.io][terraform]

[terraform]: https://terraform.io/
[terraform-providers]: https://registry.terraform.io/browse/providers
[terraform-provider-aws]: https://registry.terraform.io/providers/hashicorp/aws
[terraform-provider-azure]: https://registry.terraform.io/providers/hashicorp/azure
[terraform-provider-gcloud]: https://registry.terraform.io/providers/hashicorp/google
[terraform-provider-kubernetes]: https://registry.terraform.io/providers/hashicorp/kubernetes