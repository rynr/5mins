---
title: "Version Control System"
date: 2023-02-04T14:17:22+01:00
tags:
 - version control system
summary: A system to manage changes to computer programs, documents or other digital information.
---

## What is a `Version Control System` used for?

A Version Control System is used to manage changes to computer programs, documents or other digital information.  
It can just be a directory structure where versioning is done manually, or a system for colloborative work with lots of automation and support logic.  
Besides of managing changes, it also keeps the complete history, to be able to retrieve previous states or provide the auditability of the changes.

## Which main types of `Version Control Systems` are there.

The different systems can generally be classify their repository model as `distributed` or `client-server` and the concurrency model as `merge` or `lock` (or a combination).

 * **Distributed** means, any copy of the repository is a standalone repository on itself.
 * **Client-Server** means, there's a central repository, any local copy is just a copy.
 * **Merge** means, concurrent changes need to be merged when brought together.
 * **Lock** means, modifying a file will lock it for others.

## (Curated) list of more known `Version Control Systems`:

| System     |Repository Model|Concurrent Model|
|------------|----------------|----------------|
| CVS        | Client-Server  | Merge          |
| Darcs      | Distributed    | Merge          |
| [Git][git] | Distributed    | Merge          |
| Mercurial  | Distributed    | Merge          |
| Subversion | Client-Server  | Merge or Lock  |

[git]: {{< ref "git" >}}