---
title: "Git Clone (from the commandline)"
date: 2023-02-03T22:19:00+01:00
tags:
 - version control system
 - git
 - git clone
topic: terraform
summary: When working together with other people on a project using `git` as version control system, the first thing you need to do is retrieve the repository.
---
## Why cloning?

To work on a project which uses git as version control system, you need a local copy of the repository. You can create a new one, or you need to `clone` a repository.  
When working with a `git` repository, you usually work on your local repository. You can later push your changes to another repository or pull/fetch any updates that others pushed.

## What does cloning of a `git` repository mean?

Cloning means making a local copy of all the information of a repository.

## What do I need to clone a `git` repository?

You need to have `git` installed, and you need to know a `git url` of a repository, you have access to.  
Usually, a `git url` looks like these (optional parts in square brackets):

 * `ssh://[user@]host.xz[:port]/path/to/repo.git/` or `[user@]host.xz[:port]/path/to/repo.git/`
 * `git://host.xz[:port]/path/to/repo.git/`
 * `http[s]://host.xz[:port]/path/to/repo.git/`

## Cloning (from the commandline)

There are tools to clone a git repository, but this is how to clone it from the commandline.  
To clone a git repository, change on the commandline into a directory, where you want to create the git repository and call `git clone [user@]host.xz[:port]/path/to/repo.git/`.  
Cloning a git repository could look like this:

```
$ git clone https://github.com/git/git.git
Cloning into 'git'...
remote: Enumerating objects: 340908, done.
remote: Total 340908 (delta 0), reused 0 (delta 0), pack-reused 340908
Receiving objects: 100% (340908/340908), 210.15 MiB | 15.8 MiB/s, done.
Resolving deltas: 100% (255168/255168), done.
$ 
```

## More

 * [git-scm.com][git]
 * [git clone documentation][git-clone]

[git]: https://git-scm.com/
[git-clone]: https://git-scm.com/docs/git-clone