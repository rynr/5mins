---
title: "Docker"
tags:
 - docker
 - virtualization
summary: Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.
---
## What is Docker?

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.

In Comparison to a virtual machine, docker allows to virtualize an environment for tasks without a complete virtual machine. It allows to restrict access to filesystems and networking. The task executed is defined by a container image and a command.

## Docker Architecture

Docker runs as a daemon on a real machine. The daemon handles containers and images. Images are a layerd collection of files and some configuration. Containers are processes executed using the files and configuration of an image.  
The daemon receives it's commands from a client. It downloads images from the registry (or deletes them). It starts/stops/removes containers on request and configures them.  
Another topic are resources like filesystems and networking for the containers. Containers are generally restricted in their access, may be put into networks.

```goat
+-------------+   +-------------------------------------------+   +----------------+
|    Client   |   |                    Host                   |   |    Registry    |
|             |   |                                           |   |                |
|  .-------.  |   |  .-------------------------------------.  |   |  .----------.  |
| | C:>     +-------+                 daemon                | |   | | Image 1    | |
| +---------+ |   |  '-------------------------------------'  |   |  '----------'  |
| |         | |   |                                           |   |  .----------.  |
|  '-------'  |   | +-----------------+   +-----------------+ |   | | Image 2    | |
|             |   | |   Containers    |   |     Images      | |   |  '----------'  |
|             |   | |  .-----------.  |   |  .-----------.  | |   |  .----------.  |
|             |   | | | Container 1 +---+---+ Image 1     | | |   | | Image 3    | |
|             |   | |  '-----------'  | | |  '-----------'  | |   |  '----------'  |
|             |   | |  .-----------.  | | |  .-----------.  | |   |  .----------.  |
|             |   | | | Container 2 +--'  | | Image 2     | | |   | | Image 4    | |
|             |   | |  '-----------'  |   |  '-----------'  | |   |  '----------'  |
|             |   | |  .-----------.  |   |  .-----------.  | |   |  .----------.  |
|             |   | | | Container 3 +-------+ Image 3     | | |   | | Image 5    | |
|             |   | |  '-----------'  |   |  '-----------'  | |   |  '----------'  |
|             |   | +-----------------+   +-----------------+ |   |      .….       |
+-------------+   +-------------------------------------------+   +----------------+
```