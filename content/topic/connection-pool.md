---
title: "Connection Pool"
tags:
 - database
 - database-connection
 - connection-pool
summary: A connection pool is a cache for database connections.
---
## What is a connection pool used for?

When an application connects to a database, it potentially needs to have more than one parallel connection to the database. This can be implemented by creating a new connection when a connection is required and closing it, once the connection is not needed any more.  
This has some downsides. It's mostly that connecting to the database takes some time.

## Example

A `Module` receives a request and needs to run a query against the database. It is connected to the connection pool and asks for a database connection. Usually there are idle database connections available and the `Connection Pool` will lease a connection to the `Module`. Once done with the requests, the `Module` gives back the database connection, which then can be used for other modules.

```goat
                 .--------------------.
                | Connection Pool      |
                |     +--------------+ |
  Request       |   .-+ Connection 1 +----.  
     |          |  |  +--------------+ |   |    .--------.
     v          |  |                   |    '--+          |
 .------.       |  |  +--------------+ |       |'--------'|
| Module +----->+-'   | Connection 2 +---------+          |
 '------'       |     +--------------+ |       | Database |
                |                      |    .--+          |
                |     +--------------+ |   |    '--------'
                |     | Connection 3 +----'
                |     +--------------+ |
                 '--------------------'
```
